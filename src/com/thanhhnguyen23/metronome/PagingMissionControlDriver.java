package com.thanhhnguyen23.metronome;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.json.simple.JSONObject;

public class PagingMissionControlDriver {

	private static FileReader input;

	public static void main(String[] args) throws IOException {
		System.out.println("Paging Mission Control App started ...");
		
		readFile();
		
	}

	public static void readFile() throws IOException {
		try (Scanner sc = new Scanner(new File("input.txt"))) {

			JSONObject battObj = new JSONObject();
			final JSONObject tstatObj = new JSONObject();

			List<JSONObject> alert = new ArrayList<>();
			
			Integer TSTATCounter = 0;
			Integer BATTCounter = 0;
			
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				String[] columns = line.split("\\|");


				TSTATCounter = processingSTAT(tstatObj, alert, TSTATCounter, columns);
				BATTCounter = processingBATT(battObj, alert, BATTCounter, columns);
			} 

			System.out.println(alert);
		}
	}

	private static Integer processingBATT(JSONObject battObj, List<JSONObject> alert, Integer BATTCounter,
			String[] columns) {
		if(columns[7].contains("BATT") && columns[1].contains("1000")) {
			if(rawValueIsLessThanRedLowLimit(columns)) {
				BATTCounter++; 

				if(BATTCounter.equals(1)) {
			      battObj.put("satelliteId", columns[1]);
			      battObj.put("severity", "RED LOW");
			      battObj.put("component", columns[7]);
			      battObj.put("timestamp", columns[0]);
			      
			      alert.add(battObj);
				}
			}
		}
		return BATTCounter;
	}

	private static Integer processingSTAT(final JSONObject tstatObj, List<JSONObject> alert, Integer TSTATCounter,
			String[] columns) {
		if(columns[7].contains("TSTAT") && columns[1].contains("1000")) {

			if(rawValueIsGreaterThanRedHighLimit(columns)) {
				TSTATCounter++;

				if(TSTATCounter.equals(1)) {
					tstatObj.put("satelliteId", columns[1]);
					tstatObj.put("severity", "RED HIGH");
					tstatObj.put("component", columns[7]);
					tstatObj.put("timestamp", columns[0]);

					alert.add(tstatObj);
				}
			}
		}
		return TSTATCounter;
	}

	private static boolean rawValueIsGreaterThanRedHighLimit(String[] columns) {
		return Double.parseDouble(columns[6]) > Double.parseDouble(columns[2]);
	}

	private static boolean rawValueIsLessThanRedLowLimit(String[] columns) {
		return Double.parseDouble(columns[6]) < Double.parseDouble(columns[5]);
	}	
	
}
